<?php


namespace App\Services;


use App\Models\Cursi;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageService
{
    static function add(Request $request){
        $model=new Page();
        $model->slug=$request->input('slug');
        $model->title=$request->input('name');
        $model->meta1=$request->input('name');
        $model->html_code=$request->input('html_code');
        $model->save();
    }

    static function update(Request $request){
        $model=Page::find($request->input('id'));
        $model->slug=$request->input('slug');
        $model->title=$request->input('name');
        $model->meta1=$request->input('name');
        $model->html_code=$request->input('html_code');
        $model->save();
    }

}
