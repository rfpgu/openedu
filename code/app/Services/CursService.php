<?php


namespace App\Services;
use App\Models\Bcursi;
use App\Models\Cursi;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CursService
{
    /**
     * Создаем новый курс
     * @param Request $request
     */
    static function add(Request $request){
        $model=new Cursi();
        $model->slug=$request->input('slug');
        $model->name=$request->input('name');
        $model->about_curs=$request->input('about_curs');
        $model->about_avtor=$request->input('about_avtor');
        if($request->file('image_file')){
            $file = $request->file('image_file');
            $roundName=Str::random(10).".". $file->extension();
            $destinationPath = 'cursi';
            $file->move($destinationPath,$roundName);
            $model->image_file=$roundName;
        }
        $model->save();
    }

    /**
     * Обновляем курс
     * @param Request $request
     */
    static function update(Request $request){
        $model=Cursi::find($request->input('id'));
        $model->slug=$request->input('slug');
        $model->name=$request->input('name');
        $model->about_curs=$request->input('about_curs');
        $model->about_avtor=$request->input('about_avtor');
        if($request->file('image_file')){
            $file = $request->file('image_file');
            $roundName=Str::random(10).".". $file->extension();
            $destinationPath = 'cursi';
            $file->move($destinationPath,$roundName);
            $model->image_file=$roundName;
        }
        $model->save();
    }


    static function add_lesson(Request $request){
        $max=Bcursi::where('cursi_id',$request->input('cursi_id'))->max('pos');
        $model=new Bcursi();
        $model->cursi_id=$request->input('cursi_id');
        $model->type=$request->input('type');
        $model->text_html=$request->input('text_html');
        $model->name=$request->input('name');
        $model->about=$request->input('about');
        $model->pos=(is_null($max)?1:$max+1);
        $model->save();
    }

}
