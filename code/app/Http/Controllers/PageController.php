<?php

namespace App\Http\Controllers;

use App\Models\Bcursi;
use App\Models\Cursi;
use App\Models\Page;
use App\Services\PageService;
use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public  function index(){
        $dataCurs=Cursi::all();
        $dataVideos=Bcursi::where('type','video')->orderby('id','desc')->limit(12)->get();
        return view('main',['dataCurs'=>$dataCurs,'dataVideos'=>$dataVideos]);
    }

    public function cours($slug){
        $dataCurs=Cursi::where('slug',$slug)->first();
        if(is_null($dataCurs)){
            abort(404);
        }
        $dataVideos=Bcursi::where('cursi_id',$dataCurs->id)->get();
        return view('showcurs',['dataCurs'=>$dataCurs,'dataVideos'=>$dataVideos]);
    }

    public  function coursall(){
        $dataCurs=Cursi::all();
        return view('allcurs',['dataCurs'=>$dataCurs]);
    }

    public function adminindex(){
        $this->authorize('is_admin', User::class);
        $datas=Page::all();
        return view('upanel.pages.index',['datas'=>$datas]);
    }

    public function adminadd(){
        $this->authorize('is_admin', User::class);
        return view('upanel.pages.create');
    }

    public function adminstore(Request $request){
        $this->authorize('is_admin', User::class);
        PageService::add($request);
        return redirect()->route('admin.pagelist');
    }

    public function adminedit($id){
        $this->authorize('is_admin', User::class);
        $data=Page::find($id);
        return view('upanel.pages.edit',['data'=>$data]);
    }


    public function adminupdate(Request $request){
        $this->authorize('is_admin', User::class);
        PageService::update($request);
        return redirect()->route('admin.pagelist');
    }

    public function show($slug){
        $data=Page::where('slug',$slug)->first();
        if(is_null($data))
            abort(404);
        return view('page',['data'=>$data]);
    }


}
