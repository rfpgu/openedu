<?php

namespace App\Http\Controllers;

use App\Models\Bcursi;
use Illuminate\Http\Request;

class BcursiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bcursi  $bcursi
     * @return \Illuminate\Http\Response
     */
    public function show(Bcursi $bcursi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bcursi  $bcursi
     * @return \Illuminate\Http\Response
     */
    public function edit(Bcursi $bcursi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bcursi  $bcursi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bcursi $bcursi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bcursi  $bcursi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bcursi $bcursi)
    {
        //
    }
}
