<?php

namespace App\Http\Controllers;

use App\Models\Bcursi;
use App\Models\Cursi;
use App\Services\CursService;
use App\User;
use Illuminate\Http\Request;

class CursiController extends Controller
{
    /*
     * Список курсов
     */
    public function adminindex()
    {
        $this->authorize('is_admin', User::class);
        $datas=Cursi::all();
        return view('upanel.curs.index',['datas'=>$datas]);
    }

    /**
     * Форма создания
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function admincreate(){
        return view('upanel.curs.create');
    }

    /**
     * Процесс добавления
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function adminstore(Request $request){
        CursService::add($request);
        return redirect()->route('admin.curslist');
    }

    /**
     * Форма редактирования
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminedit($id){
        $data=Cursi::find($id);
        $data_elements=Bcursi::where('cursi_id',$id)->get();
        return view('upanel.curs.edit',['data'=>$data,'data_elements'=>$data_elements]);
    }


    public function adminupdate(Request $request){
        CursService::update($request);
        return redirect()->route('admin.curslist');
    }

    public function adminstore_curs(Request $request){
        CursService::add_lesson($request);
        return redirect()->back();
    }

}
