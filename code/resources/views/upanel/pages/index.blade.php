@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('message.admin_curs_index') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <a href="{{ route('admin.pagelist.add') }}">{{ __('message.admin_add') }}</a>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('message.admin_page__id') }}</th>
                                <th scope="col">{{ __('message.admin_page_table_slug') }}</th>
                                <th scope="col">{{ __('message.admin_page_table_name') }}</th>
                                <th scope="col">{{ __('message.admin_page_table_event') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datas as $data)
                                <tr>
                                    <th scope="row">{{$data->id}}</th>
                                    <td>{{$data->slug }}</td>
                                    <td>{{$data->title }}</td>
                                    <td>
                                        <a href="{{route('admin.pagelist.edit',['id'=>$data->id])}}">
                                            {{ __('message.admin_edit') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
