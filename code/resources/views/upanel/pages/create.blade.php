@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('message.admin_pages_create') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.pagelist.store') }}"
                              method="post"
                              enctype="multipart/form-data"
                            >
                            <div class="form-group">
                                <label for="idNameCurs">Название страницы</label>
                                <input type="text"
                                       name="name"
                                       class="form-control" id="idNameCurs">
                            </div>
                            <div class="form-group">
                                <label for="idSlugCurs">ЧПУ страницы</label>
                                <input type="text"
                                       name="slug"
                                       class="form-control" id="idSlugCurs">
                            </div>
                            <div class="form-group">
                                <label for="editor">Описание курса</label>
                                <textarea class="form-control"
                                          name="html_code"
                                          id="itAboutCurs" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Создать страницы</button>
                            </div>
                            @csrf
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
