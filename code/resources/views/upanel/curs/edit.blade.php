@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('message.admin_curs_create') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.curslist.updatecurs') }}"
                              method="post"
                              enctype="multipart/form-data"
                            >
                            <div class="form-group">
                                <label for="idNameCurs">Название курса</label>
                                <input type="text"
                                       name="name"
                                       value="{{$data->name}}"
                                       class="form-control" id="idNameCurs">
                            </div>
                            <div class="form-group">
                                <label for="idSlugCurs">ЧПУ курса</label>
                                <input type="text"
                                       name="slug"
                                       value="{{$data->slug}}"
                                       class="form-control" id="idSlugCurs">
                            </div>
                            <div class="form-group">
                                <label for="idAutorCurs">Автор курса</label>
                                <input type="text"
                                       name="about_avtor"
                                       value="{{$data->about_avtor}}"
                                       class="form-control" id="idAutorCurs">
                            </div>
                            <div class="form-group">
                                <label for="editor">Описание курса</label>
                                <textarea class="form-control"
                                          name="about_curs"
                                          id="itAboutCurs" rows="10">{!!$data->about_curs!!}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Фото курса (1430*230)</label>
                                <input type="file"
                                       name="image_file"
                                       class="form-control-file" id="exampleFormControlFile1">
                                <img src="{{ asset('cursi/'.$data->image_file)}}"
                                    width="400px"
                                    >
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Обновить курс</button>
                            </div>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            @csrf
                        </form>
                    </div>

                    <div class="accordion" id="accordionExample">



                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target="#add_new"
                                            aria-expanded="true"
                                            aria-controls="collapseOne">
                                        Создать элемент в курсе
                                    </button>
                                </h2>
                            </div>

                            <div id="add_new"
                                 class="collapse"
                                 aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <form action="{{ route('admin.curslist.store_curs') }}"
                                          method="post"
                                          enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="newIdName">Название</label>
                                            <input type="text"
                                                   class="form-control"
                                                   id="newIdName"
                                                   name="name"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <label for="newIdAbout">Описание</label>
                                            <input type="text"
                                                   class="form-control"
                                                   id="newIdAbout"
                                                   name="about"
                                            >
                                        </div>


                                        <div class="form-group">
                                            <label for="idType">Тип</label>
                                            <select class="form-control" id="idType" name="type">
                                                <option value="">Выберите категорию</option>
                                                <option value="text">Текст</option>
                                                <option value="video">Видео</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="newIdAbout">Текст/Видео</label>
                                            <textarea class="form-control"
                                                      name="text_html"
                                                      id="bCurs" rows="10"></textarea>
                                        </div>

                                        <input type="hidden" name="cursi_id" value="{{$data->id}}">
                                        @csrf

                                        <button type="submit" class="btn btn-primary">Создать данные</button>
                                    </form>
                                </div>
                            </div>
                        </div>




                        @foreach($data_elements as $data_element)
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target="#edit_id{{$data_element->id}}"
                                            aria-expanded="true"
                                            aria-controls="collapseOne">
                                        {{$data_element->name}} (id-{{$data_element->id}})
                                    </button>
                                </h2>
                            </div>

                            <div id="edit_id{{$data_element->id}}"
                                 class="collapse"
                                 aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="newIdName">Название</label>
                                            <input type="text"
                                                   readonly
                                                   class="form-control"
                                                   id="newIdName"
                                                   value="{{$data_element->name}}"
                                                   name="name"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <label for="newIdAbout">Описание</label>
                                            <input type="text"
                                                   readonly
                                                   class="form-control"
                                                   id="newIdAbout"
                                                   name="about"
                                                   value="{{$data_element->about}}"
                                            >
                                        </div>


                                        <div class="form-group">
                                            <label for="idType">Тип</label>
                                            <select class="form-control" id="idType" name="type" readonly>
                                                <option value="text"
                                                    @if($data_element->type=='text')
                                                        selected
                                                    @endif
                                                >Текст</option>
                                                <option value="video"
                                                    @if($data_element->type=='video')
                                                        selected
                                                    @endif
                                                >Видео</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="newIdAbout">Текст/Видео</label>
                                            <textarea class="form-control"
                                                      readonly="readonly"
                                                      rows="10">{!!$data_element->text_html!!}</textarea>
                                        </div>

                                        <input type="hidden" name="cursi_id" value="{{$data->id}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
