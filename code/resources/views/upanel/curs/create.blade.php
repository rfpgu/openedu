@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('message.admin_curs_create') }}</div>

                    <div class="card-body">
                        <form action="{{ route('admin.curslist.store') }}"
                              method="post"
                              enctype="multipart/form-data"
                            >
                            <div class="form-group">
                                <label for="idNameCurs">Название курса</label>
                                <input type="text"
                                       name="name"
                                       class="form-control" id="idNameCurs">
                            </div>
                            <div class="form-group">
                                <label for="idSlugCurs">ЧПУ курса</label>
                                <input type="text"
                                       name="slug"
                                       class="form-control" id="idSlugCurs">
                            </div>
                            <div class="form-group">
                                <label for="idAutorCurs">Автор курса</label>
                                <input type="text"
                                       name="about_avtor"
                                       class="form-control" id="idAutorCurs">
                            </div>
                            <div class="form-group">
                                <label for="editor">Описание курса</label>
                                <textarea class="form-control"
                                          name="about_curs"
                                          id="itAboutCurs" rows="10"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlFile1">Фото курса (1430*230)</label>
                                <input type="file"
                                       name="image_file"
                                       class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Обновить курс</button>
                            </div>
                            @csrf
                        </form>






                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
