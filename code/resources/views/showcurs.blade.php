@extends('part.main')

@section('content')



    <main class="main">
        <div class="row">
                <div class="info_for_kurs">
                    <p class="info_curs_title">
                        {{$dataCurs->name}}
                    </p>
                    <hr>
                    <div class="info_curs_about">
                        {!!  $dataCurs->about_curs!!}
                    </div>

                </div>
        </div>

        <div class="row main_video">
            <p class="main_viedo-p">
                Элементы курса
            </p>
        </div>

        <div class="row">
            @foreach($dataVideos as $dataVideo)
                <div class="card_for_kurs">
                    <?php
                    $pos=strripos($dataVideo->text_html,'url="');
                    $pos1=strripos($dataVideo->text_html,'"></oembed></figure>');
                    $url=substr($dataVideo->text_html,$pos+5,$pos1-$pos-5);
                    $posv=strripos($url,'v=');
                    $posv1=strripos($url,'&amp;');
                    if($posv1==0){
                        $codeActive=substr($url,$posv);
                    }else{
                        $codeActive=substr($url,$posv+2,$posv1-$posv-2);
                    }

                    $posv=strripos($codeActive,'v=');
                    //dump($codeActive);
                    if($posv1==0){
                        $part = explode("v=", $codeActive);
                        if(count($part)==1){
                            $codeActive=$part[0];
                        }else{
                            $part = explode("&list", $part[1]);
                            $codeActive=$part[0];
                        }

                    }

                    ?>
                    <img src="//img.youtube.com/vi/{{$codeActive}}/default.jpg"
                             data-linkurl="{{$codeActive}}"
                         data-nameurl="{{$dataVideo->name}}"
                         data-about="{{$dataVideo->about}}"
                         class="card_image youtobeimg">
                    <p class="card_text">
                        {{$dataVideo->name}}
                    </p>
                    <center>
                        {{\Carbon\Carbon::parse($dataVideo->created_at)->format('Y-m-d h:m:s')}}
                    </center>
                </div>
        @endforeach
    </main>
    <br><br><br>
    <footer class="footer">
        <div class="row footer-line">
        </div>
        <div class="row footer-line1">
            <div class="foot_part1">


                <div class="link_foot_1">
                    <img src="/img/svg/1.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/2.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/3.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/4.svg" alt="" class="link_footer_img">

                </div>
                <div class="link_foot_2">
                    <img src="/img/svg/Line1.svg"
                         alt="" class="link_footer_img">
                </div>
                <div class="link_foot_3">
                    <p  class="text_f1">"ОТКРЫТОЕ ОБРАЗОВАНИЕ"</p>
                    <p  class="text_f2">ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                    <p  class="text_f2">УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                </div>
            </div>
        </div>
        <div class="row footer-line2">
        </div>
    </footer>





    <div class="modal-overlay closed" id="modal-overlay"></div>
    <div class="modal closed" id="modal">
        <button class="close-button" id="close-button">Закрыть</button>
        <div class="modal-guts">
            <h1 id="showName">Modal Example</h1>
            <br>
            <p id="showPlaer"></p>
            <p id="showAbout"></p>

        </div>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $( ".youtobeimg" ).click(function() {
            modal.classList.toggle("closed");
            modalOverlay.classList.toggle("closed");
            var youtobe_code=$(this).data("linkurl");
            var text='<iframe width="560" height="315" src="https://www.youtube.com/embed/'+youtobe_code+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            document.getElementById('showPlaer').innerHTML=text;
            document.getElementById('showName').innerHTML=$(this).data("nameurl");
            document.getElementById('showAbout').innerHTML=$(this).data("about");
        });

        var modal = document.querySelector("#modal");
        var modalOverlay = document.querySelector("#modal-overlay");
        var closeButton = document.querySelector("#close-button");
        var openButton = document.querySelector("#open-button");

        closeButton.addEventListener("click", function() {
            modal.classList.toggle("closed");
            modalOverlay.classList.toggle("closed");
        });

    </script>
@endsection
