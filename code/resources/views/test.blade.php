<html>
    <head>
        <meta charset="UTF-8">
        <title>'ОТКРЫТОЕ ОБРАЗОВАНИЕ' ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</title>
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="./css/reset.css">
        <link rel="stylesheet" href="./css/style.css?v=2">

    </head>
    <body>
        <header class="header">
            <div class="wrapper">
                <div class="header__wrapper">
                    <div class="header__logo">
                        <a href="/" class="header__logo-link">
                            <img src="./img/svg/logo.svg"
                                 alt="'ОТКРЫТОЕ ОБРАЗОВАНИЕ' ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ"
                                 class="header__logo-pic">
                        </a>
                    </div>
                    <div class="header__text_logo">
                        <p class="big__text">'ОТКРЫТОЕ ОБРАЗОВАНИЕ'</p>
                        <p>ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                        <p>УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                    </div>

                    <nav class="header__nav">
                        <ul class="header__list">
                            <li class="header__item">
                                <a href="#!" class="header__link">
                                    Курсы
                                </a>
                            </li>
                        </ul>
                    </nav>


                </div>

            </div>
        </header>
        <div>
            <img src="./img/slider.jpg" class="slider_div">
        </div>

        <main class="main">
            <div class="row">
                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название курса
                    </p>
                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название курса
                    </p>
                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название курса
                    </p>
                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название курса
                    </p>
                </div>
            </div>

            <div class="row main_video">
                <p class="main_viedo-p">
                    Последние видео
                </p>

            </div>

            <div class="row">
                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название видео
                    </p>
                    <center>
                        22/01/2020
                    </center>
                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название видео
                    </p>
                    <center>
                        22/01/2020
                    </center>

                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название видео
                    </p>
                    <center>
                        22/01/2020
                    </center>
                </div>

                <div class="card_for_kurs">
                    <img src="/img/svg/tetrad.svg" alt="Название курса" class="card_image">
                    <p class="card_text">
                        Название видео
                    </p>
                    <center>
                        22/01/2020
                    </center>
                </div>
            </div>

        </main>
        <br><br><br>
        <footer class="footer">
            <div class="row footer-line">
            </div>
            <div class="row footer-line1">
                <div class="foot_part1">


                    <div class="link_foot_1">
                        <img src="/img/svg/1.svg" alt="" class="link_footer_img">
                        <img src="/img/svg/2.svg" alt="" class="link_footer_img">
                        <img src="/img/svg/3.svg" alt="" class="link_footer_img">
                        <img src="/img/svg/4.svg" alt="" class="link_footer_img">

                    </div>
                    <div class="link_foot_2">
                        <img src="/img/svg/Line1.svg" alt="" class="link_footer_img">
                    </div>
                    <div class="link_foot_3">
                        <p  class="text_f1">"ОТКРЫТОЕ ОБРАЗОВАНИЕ"</p>
                        <p  class="text_f2">ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                        <p  class="text_f2">УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                    </div>
                </div>
            </div>
            <div class="row footer-line2">

            </div>
        </footer>



    </body>
</html>
