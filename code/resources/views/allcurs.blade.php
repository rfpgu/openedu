@extends('part.main')

@section('content')

    <div>

        <img src="{{asset('/img/slider2.png')}}" class="slider_div">
    </div>

    <main class="main">
        <div class="row">
            @foreach($dataCurs as $dataCur)
                <div class="card_for_kurs">
                    <a href="{{ route('site.showcurs',['slug'=>$dataCur->slug]) }}">
                    <img src="{{ asset('cursi/'.$dataCur->image_file)}}"
                         alt="Название курса" class="card_image">
                    <p class="card_text">
                        {{$dataCur->name}}
                    </p></a>
                </div>
            @endforeach
        </div>
    </main>
    <br><br><br>
    <footer class="footer">
        <div class="row footer-line">
        </div>
        <div class="row footer-line1">
            <div class="foot_part1">


                <div class="link_foot_1">
                    <img src="/img/svg/1.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/2.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/3.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/4.svg" alt="" class="link_footer_img">

                </div>
                <div class="link_foot_2">
                    <img src="/img/svg/Line1.svg"
                         alt="" class="link_footer_img">
                </div>
                <div class="link_foot_3">
                    <p  class="text_f1">"ОТКРЫТОЕ ОБРАЗОВАНИЕ"</p>
                    <p  class="text_f2">ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                    <p  class="text_f2">УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                </div>
            </div>
        </div>
        <div class="row footer-line2">
        </div>
    </footer>





    <div class="modal-overlay closed" id="modal-overlay"></div>
    <div class="modal closed" id="modal">
        <button class="close-button" id="close-button">Закрыть</button>
        <div class="modal-guts">
            <h1 id="showName">Modal Example</h1>
            <br>
            <p id="showPlaer"></p>
            <p id="showAbout"></p>

        </div>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $( ".youtobeimg" ).click(function() {
            modal.classList.toggle("closed");
            modalOverlay.classList.toggle("closed");
            var youtobe_code=$(this).data("linkurl");
            var text='<iframe width="560" height="315" src="https://www.youtube.com/embed/'+youtobe_code+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            document.getElementById('showPlaer').innerHTML=text;
            document.getElementById('showName').innerHTML=$(this).data("nameurl");
            document.getElementById('showAbout').innerHTML=$(this).data("about");
        });

        var modal = document.querySelector("#modal");
        var modalOverlay = document.querySelector("#modal-overlay");
        var closeButton = document.querySelector("#close-button");
        var openButton = document.querySelector("#open-button");

        closeButton.addEventListener("click", function() {
            modal.classList.toggle("closed");
            modalOverlay.classList.toggle("closed");
        });

    </script>
@endsection
