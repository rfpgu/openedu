@extends('part.main')

@section('content')



    <main class="main">
        <div class="row">
            <div class="info_for_kurs">
                <p class="info_curs_title">
                    {{$data->title}}
                </p>
                <hr>
                <div class="info_curs_about">
                    {!! $data->html_code!!}
                </div>

            </div>
        </div>
    </main>
    <br><br><br>
    <footer class="footer">
        <div class="row footer-line">
        </div>
        <div class="row footer-line1">
            <div class="foot_part1">


                <div class="link_foot_1">
                    <img src="/img/svg/1.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/2.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/3.svg" alt="" class="link_footer_img">
                    <img src="/img/svg/4.svg" alt="" class="link_footer_img">

                </div>
                <div class="link_foot_2">
                    <img src="/img/svg/Line1.svg"
                         alt="" class="link_footer_img">
                </div>
                <div class="link_foot_3">
                    <p  class="text_f1">"ОТКРЫТОЕ ОБРАЗОВАНИЕ"</p>
                    <p  class="text_f2">ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                    <p  class="text_f2">УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                </div>
            </div>
        </div>
        <div class="row footer-line2">
        </div>
    </footer>

@endsection
