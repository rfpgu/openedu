<html>
<head>
    <meta charset="UTF-8">
    <title>'ОТКРЫТОЕ ОБРАЗОВАНИЕ' ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/reset.css?v=5')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css?v=5')}}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-JPB99QPK1M"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-JPB99QPK1M');
    </script>
</head>
<body>
<header class="header">
    <div class="wrapper">
        <div class="header__wrapper">
            <div class="header__logo">
                <a href="/" class="header__logo-link">
                    <img src="{{asset('/img/svg/logo.svg')}}"
                         alt="'ОТКРЫТОЕ ОБРАЗОВАНИЕ' ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ"
                         class="header__logo-pic">
                </a>
            </div>
            <div class="header__text_logo">
                <a href="/" class="header__text_logo">
                    <p class="big__text">'ОТКРЫТОЕ ОБРАЗОВАНИЕ'</p>
                    <p>ПРИДНЕСТРОВСКИЙ ГОСУДАРСТВЕННЫЙ</p>
                    <p>УНИВЕРСИТЕТ им. Т.Г.ШЕВЧЕНКО в г. РЫБНИЦЕ</p>
                </a>
            </div>

            <nav class="header__nav">
                <ul class="header__list">

                    <li class="header__item">

                        <a href="http://rfpgu.ru/" class="header__link">
                           Сайт РФ ПГУ
                        </a>
                    </li>
                </ul>
            </nav>


        </div>

    </div>
</header>

@yield('content')


</body>
</html>
