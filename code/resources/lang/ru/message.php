<?php

return [
    'menu_curs' => 'БД курсов',
    'admin_curs_index' => 'Список курсов',
    'admin_curs_create' => 'Создать курс ',
    'admin_curs_table_id' => 'Номер курса',
    'admin_curs_table_slug' => 'ЧПУ курса',
    'admin_curs_table_name' => 'Название курса',
    'admin_curs_table_event'=>'Действие',
    'admin_edit'=>'редактировать',
    'admin_delet'=>'удалить',
    'admin_add'=>'создать',
    'menu_page' => 'БД страниц',
    'admin_page__id' => 'Номер страницы',
    'admin_page_table_slug' => 'ЧПУ страницы',
    'admin_page_table_name' => 'Название страницы',
    'admin_page_table_event'=>'Действие',
    'admin_pages_create'=>'Добавить страницу'
];

