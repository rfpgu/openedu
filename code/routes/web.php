<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@index');
Route::get('/cours/','PageController@coursall')->name('site.allcurs');
Route::get('/cours/{slug}','PageController@cours')->name('site.showcurs');
Route::get('/page/{slug}','PageController@show')->name('site.page');

Route::get('/course/moodle', function () {
    return view('test.moodle');
});

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::prefix('upaneladmin')->group(function () {
        Route::get('curs', 'CursiController@adminindex')->name('admin.curslist');
        Route::get('curs/add', 'CursiController@admincreate')->name('admin.curslist.add');
        Route::get('curs/{id}', 'CursiController@adminedit')->name('admin.curslist.edit');
        Route::post('curs/store', 'CursiController@adminstore')->name('admin.curslist.store');
        Route::post('curs/update', 'CursiController@adminupdate')->name('admin.curslist.updatecurs');
        Route::post('curs/bcurs_store', 'CursiController@adminstore_curs')->name('admin.curslist.store_curs');

        Route::get('page', 'PageController@adminindex')->name('admin.pagelist');
        Route::get('page/add', 'PageController@adminadd')->name('admin.pagelist.add');
        Route::post('page/store', 'PageController@adminstore')->name('admin.pagelist.store');
        Route::get('page/edit/{id}', 'PageController@adminedit')->name('admin.pagelist.edit');
        Route::post('page/update', 'PageController@adminupdate')->name('admin.pagelist.update');
    });
});
